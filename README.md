# VM ansible playbooks

A collection of [Ansible playbook](https://docs.ansible.com/ansible/latest/user_guide/playbooks.html) to install software on GRADES Virtual Machines (VM). 

These playbooks allow to run recipes to install stuff after the creation of the VM via:
https://docs.ansible.com/ansible/latest/user_guide/playbooks.html

# Run ansible with a given playbook name

Playbook are loaded directly from this git repo thanks to ansible-pull command. 

List of available recipes:
* [Napari](#napari)
* [Fiji](#fiji)

## Napari

```bash
ansible-pull -U https://gitlab.synchrotron-soleil.fr/disco-beamline/vm-ansible-playbooks.git napari.yml
```

After some time you will see an icon Napari on the desktop, just double click on it to launch Napari.

## Fiji

```bash
ansible-pull -U https://gitlab.synchrotron-soleil.fr/disco-beamline/vm-ansible-playbooks.git fiji.yml
```
After some time you will see an icon Fiji on the desktop, just double click on it to launch Fiji.
